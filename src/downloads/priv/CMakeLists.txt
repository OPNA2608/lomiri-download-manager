set(TARGET lomiri-download-manager-priv)

set(SOURCES
	lomiri/downloads/daemon.cpp
	lomiri/downloads/download.cpp
	lomiri/downloads/download_adaptor.cpp
	lomiri/downloads/download_adaptor_factory.cpp
	lomiri/downloads/download_manager_adaptor.cpp
	lomiri/downloads/download_manager_factory.cpp
	lomiri/downloads/downloads_db.cpp
	lomiri/downloads/factory.cpp
	lomiri/downloads/file_download.cpp
	lomiri/downloads/group_download.cpp
	lomiri/downloads/group_download_adaptor.cpp
	lomiri/downloads/header_parser.cpp
	lomiri/downloads/manager.cpp
	lomiri/downloads/mms_file_download.cpp
	lomiri/downloads/sm_file_download.cpp
	lomiri/downloads/state_machines/download_sm.cpp
	lomiri/downloads/state_machines/final_state.cpp
	lomiri/downloads/state_machines/state.cpp
)

set(HEADERS
	lomiri/downloads/daemon.h
	lomiri/downloads/download.h
	lomiri/downloads/download_adaptor.h
	lomiri/downloads/download_adaptor_factory.h
	lomiri/downloads/download_manager_adaptor.h
	lomiri/downloads/download_manager_factory.h
	lomiri/downloads/downloads_db.h
	lomiri/downloads/factory.h
	lomiri/downloads/file_download.h
	lomiri/downloads/group_download.h
	lomiri/downloads/group_download_adaptor.h
	lomiri/downloads/header_parser.h
	lomiri/downloads/manager.h
	lomiri/downloads/mms_file_download.h
	lomiri/downloads/sm_file_download.h
	lomiri/downloads/state_machines/download_sm.h
	lomiri/downloads/state_machines/final_state.h
	lomiri/downloads/state_machines/state.h
)

include_directories(${DBUS_INCLUDE_DIRS})
include_directories(${CMAKE_CURRENT_SOURCE_DIR})
include_directories(${CMAKE_CURRENT_BINARY_DIR})
include_directories(${CMAKE_SOURCE_DIR}/src/common/public)
include_directories(${CMAKE_SOURCE_DIR}/src/common/priv)
include_directories(${CMAKE_SOURCE_DIR}/src/downloads/common)

add_definitions(-DHELPER_DIR="${CMAKE_INSTALL_FULL_LIBEXECDIR}/lomiri-download-manager")

add_library(${TARGET} STATIC
	${HEADERS}
	${SOURCES}
)

set_target_properties(
	${TARGET}

	PROPERTIES
	VERSION ${LDM_VERSION_MAJOR}.${LDM_VERSION_MINOR}.${LDM_VERSION_PATCH}
	SOVERSION ${LDM_VERSION_MAJOR}
)

target_link_libraries(${TARGET}
	${GLOG_LIBRARIES}
	Qt5::DBus
	Qt5::Sql
	ldm-common
	ldm-priv-common
	lomiri-download-manager-common
)
