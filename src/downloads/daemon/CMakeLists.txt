set(TARGET lomiri-download-manager)

set(SOURCES
	main.cpp
)

include_directories(${CMAKE_CURRENT_SOURCE_DIR})
include_directories(${CMAKE_CURRENT_BINARY_DIR})
include_directories(${CMAKE_SOURCE_DIR}/src/common/public)
include_directories(${CMAKE_SOURCE_DIR}/src/common/priv)
include_directories(${CMAKE_SOURCE_DIR}/src/downloads/common)
include_directories(${CMAKE_SOURCE_DIR}/src/downloads/priv)

add_executable(${TARGET}
	${SOURCES}
)

target_link_libraries(${TARGET}
	${GLOG_LIBRARIES}
	Qt5::Core
	ldm-common
	ldm-priv-common
	lomiri-download-manager-common
	lomiri-download-manager-priv
)

configure_file(
  ${TARGET}.service.in ${TARGET}.service @ONLY
)
configure_file(
  com.lomiri.applications.Downloader.service.in com.lomiri.applications.Downloader.service @ONLY
)

if(USE_SYSTEMD)
  configure_file(
    lomiri-download-manager-systemd.service.in lomiri-download-manager-systemd.service @ONLY
  )

  install(
    FILES ${CMAKE_CURRENT_BINARY_DIR}/lomiri-download-manager-systemd.service
    DESTINATION ${SYSTEMD_USER_DIR}
  )
endif()

install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${TARGET}.service DESTINATION
    ${CMAKE_INSTALL_FULL_DATADIR}/dbus-1/services)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/com.lomiri.applications.Downloader.service DESTINATION
    ${CMAKE_INSTALL_FULL_DATADIR}/dbus-1/system-services)
install(FILES com.lomiri.applications.Downloader.conf DESTINATION
    ${CMAKE_INSTALL_FULL_SYSCONFDIR}/dbus-1/system.d)
install(TARGETS ${TARGET} DESTINATION ${CMAKE_INSTALL_FULL_BINDIR})
