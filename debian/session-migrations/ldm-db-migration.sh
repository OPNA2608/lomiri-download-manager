#!/bin/sh -euf
#
# Copyright (C) 2023 UBports Foundation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation; version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Authored by: Ratchanan Srirattanamet <ratchanan@ubports.com>

# Note: migrate only the DB, not the files. File paths are kept in this DB, and
# let's not get into the business of modifying DB, OK?
old_path="${HOME}/.local/share/ubuntu-download-manager/ubuntu-download-manager/downloads.db"
new_path="${HOME}/.local/share/lomiri-download-manager/lomiri-download-manager/downloads.db"

if [ -e "$new_path" ]; then
    echo "${new_path} exists. Apps might have already downloaded more files." \
         "Don't migrate." >&2
    exit 0
fi

if ! [ -e "$old_path" ]; then
    echo "${old_path} doesn't exist. Nothing to do." >&2
    exit 0
fi

mkdir -p "$(dirname "$new_path")"
cp -a "${old_path}" "${new_path}"

echo "LDM download database migrated." >&2
