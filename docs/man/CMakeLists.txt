install(
  FILES
    lomiri-download-manager.1
    lomiri-upload-manager.1
  DESTINATION ${CMAKE_INSTALL_FULL_MANDIR}/man1/
)
